function updateVideoSizes(videos) {
	var videos = videos || $('#media > video:visible');
	if (videos.length < 2) {
		var rows = 1;
		var cols = 1;
	} else {
		var rows = Math.ceil(videos.length / 2);
		var cols = 2;
	}
	var w = (100.0 / cols) - 0.5;
	var h = (100.0 / rows) - 0.5;

	var style = {
		width: w + '%',
		height: h + '%'
	};
	videos.css(style);
	//videos.animate(style, 100);
}

$(function() {
	if ( !('flex-direction' in document.getElementById('media').style) ) {
		alert("Uh oh! Your browser doesn't support CSS3 and the flexbox layout needed to display this page. Try the latest version of Chrome or Safari instead.");
	}

	/// Load media data
	//TODO: eventually, when there is a client-server divide, this will use real JSON and not hack-y JSON-P
	// $.getJSON('media.json', function(loadMedia) {

		/// Initialize the synchronizer and controls
		initialize(sources, document.getElementById('media'), {
			play: document.getElementById('play'),
			scrub: document.getElementById('scrub'),
			duration: document.getElementById('duration'),
			position: document.getElementById('position')
		}, function(mediaInfo) {

			$('#media > video').click(function() {
				var hidden = $('#media > video:hidden');
				if (hidden.length > 0) {
					hidden.show();
					updateVideoSizes();
				} else {
					$('#media > video').not(this).hide();
					updateVideoSizes( $(this) );
				}
			});

			updateVideoSizes();
			makeTimeline(mediaInfo);
		});
	// });

});

function makeTimeline(mediaInfo) {
	for (t in mediaInfo.media) {
		var track = mediaInfo.media[t];
		var dom = track.dom;

		// Generate the timeline bar

		var $bar = $('<div class="timelineRow timelineBar"></div>');
		var width = 100 * ( (track.len * track.rate) / mediaInfo.timelineLength );
		var inpoint = 100 * ( (track.in * track.rate) / mediaInfo.timelineLength );

		$bar.css({
			'width': width + '%',
			'margin-left': inpoint + '%'
		});

		// Generate the sync point markers
		pulseWidth = Math.max(100 * (1.0 / mediaInfo.timelineLength), 0.2);	//1.0 sec pulses
		var syncPoints = track.syncs;
		for (var i = 0; i < syncPoints.length; i++) {
			var left = 100 * (syncPoints[i] / track.len) - (i * pulseWidth); //Each previous div's width will offset this one that far to the right
			$bar.append('<div class="syncPoint" style="left: ' + left + '%; width: ' + pulseWidth + '%;"></div>');
		}

		$('#timeline').append($bar);

		// Generate the label / track controls
		var $label = $('<label class="timelineRow timelineLabel" for="track' + t + 'muter"></label>');
		$label.text(track.name);

		var $mute = $('<input type="checkbox" class="muteBox unselectable" id="track' + t + 'muter" checked><label></label>');
		$mute.click(function(elem) {
			return function() {
				elem.muted = !this.checked;
				console.log(elem);
			};
		}(dom));

		$label.append($mute);

		$('#labels').append($label);
	}
}