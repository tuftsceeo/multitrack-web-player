var auContext;
window.addEventListener('load', function() {
  try {
    // Fix up for prefixing
    window.AudioContext = window.AudioContext||window.webkitAudioContext;
    auContext = new AudioContext();
  }
  catch(e) {
    alert('Web Audio API is not supported in this browser');
  }
}, false);

function Ducker(mediaElement, thresh, onDuck, onUnDuck) {
	// var stat = $('<p id="duckStat">');
	// $('body').append(stat);

	this.dom = mediaElement;
	this.thresh = thresh;
	this.onDuck = onDuck || null;
	this.onUnDuck = onUnDuck || null;
	this.ducked = false;

	this.sourceNode = auContext.createMediaElementSource(mediaElement);
	this.analyserNode = auContext.createAnalyser();
	this.analyserNode.smoothingTimeConstant = 0.3;
	this.analyserNode.fftSize = 1024;

	this.muteNode = auContext.createGainNode();
	this.muteNode.gain.value = 1.0;

	this.jsNode = auContext.createJavaScriptNode( this.analyserNode.fftSize*2, 1, 1 );
	this.jsNode.onaudioprocess = function(ducker) {
		return function() {
			var array = new Uint8Array(ducker.analyserNode.frequencyBinCount);
	        ducker.analyserNode.getByteFrequencyData(array);

	        var sum = 0;
	        var len = array.length;
	        for (var i = 0; i < len; i++) {
	        	sum += array[i];
	        }
	        var vol = sum / len;
	        // stat.text(vol);
	        if (vol > ducker.thresh && ducker.ducked) {
	        	ducker.ducked = false;
	        	ducker.muteNode.gain.cancelScheduledValues(0);
	        	ducker.muteNode.gain.linearRampToValueAtTime(1.0, auContext.currentTime + 0.8);
	        	if (ducker.onUnDuck) ducker.onUnDuck(vol);
	        	// stat.css('color', 'green');
	        } else if (vol < ducker.thresh && !ducker.ducked) {
	        	ducker.ducked = true;
	        	ducker.muteNode.gain.cancelScheduledValues(0);
	        	ducker.muteNode.gain.linearRampToValueAtTime(0.0, auContext.currentTime + 0.8);
	        	if (ducker.onDuck) ducker.onDuck(vol);
	        	// stat.css('color', 'red');
	        }
	    }
	}(this)

	this.sourceNode.connect(this.analyserNode);
	this.analyserNode.connect(this.muteNode);
	this.muteNode.connect(this.jsNode);
	this.muteNode.connect(auContext.destination);
	this.jsNode.connect(auContext.destination);		//must be connected to destination to execute - but it doesn't
													//write to output buffer, therefore it's silent
}