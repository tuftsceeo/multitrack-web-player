var media = [];
var timelineLength = undefined;
var play = false;
var scrub = false;
var playhead = 0;
var syncTrack = null;

var deltas = [];
var nextDeltaInd = 1;
var nextDelta = null;
var currentDelta = null;
var currentlyPlaying = {};

/// Track properties:
///		dom    -    the DOM object of the audio/video
///		name   -	the name specified by data-name
///		in     -    global in point (seconds)
///		out    -    global out point (seconds)
///		len    -    duration (seconds)
///		rate   -	rate at which track should be played to match global rate
///		syncs  -	array of timestamps when sync points (eg. 20kHz tones) occur in this track
///					(never used, just for display)

function Track(mediaDOMObject, name, syncs, in_time, rate) {
	this.dom = mediaDOMObject;
	this.name = name;
	this.syncs = syncs;
	var in_time = in_time || $(mediaDOMObject).data('in');
	// if (typeof in_time == 'undefined') in_time = 0;
	this.in = parseFloat(in_time);
	if (isNaN(in_time)) {
		console.log('Invaid in-time for track ' + $(mediaDOMObject).data('name'));
		this.in = 0;
	}
	var rate = rate || $(mediaDOMObject).data('rate');
	// if (typeof rate == 'undefined') rate = 1.0;
	this.rate = parseFloat(rate);

	this.len = this.dom.duration / this.rate;
	this.out = this.in + this.len;

	this.dom.defaultPlaybackRate = this.rate;
	this.dom.playbackRate = this.rate;
}
Track.prototype.globalTime = function(timeToSet) {
	if (timeToSet) {
		var localTime = (timeToSet - this.in) * this.rate
		this.dom.currentTime = localTime;
		return localTime;
	} else {
		return this.dom.currentTime * (1/this.rate) + this.in;
	}
};

/// PlayStateChange properties:
///		in     -    time at which this delta should be applied
///		out    -	time after which this is no longer the most recent change to apply
///		action -    'play' or 'pause'
///		track  -    the Track object to perform the action on
///		dom    -    reference to the track's dom object to speed things up

function PlayStateChange(start, end, action, track) {
	if (action != 'play' && action != 'pause') {
		console.log(action + ' is not a valid action!');
		return null;
	}
	this.in = start;
	this.out = end;
	this.action = action;
	this.track = track;
	this.dom = track.dom;
}
PlayStateChange.prototype.apply = function() {
	this.dom[ this.action ]();
	this.trackPlay();
}
PlayStateChange.prototype.unApply = function() {
	this.dom[ this.action == 'play' ? 'pause' : 'play' ]();
	this.untrackPlay();
}
PlayStateChange.prototype.trackPlay = function() {
	if (this.action == 'play') {
		currentlyPlaying[this.track.name] = this.track;		// KNOWN BUG: if two tracks have the same name, playback will be all sorts of messed up
		console.log('applying play to ' + this.track.name);
	} else {
		delete currentlyPlaying[this.track.name];
		console.log('removing play from ' + this.track.name);
	}
}
PlayStateChange.prototype.untrackPlay = function() {
	if (this.action == 'pause') {
		currentlyPlaying[this.track.name] = this.track;
		console.log('applying play to ' + this.track.name + ' to untrack a pause');
	} else {
		delete currentlyPlaying[this.track.name];
		console.log('removing play from ' + this.track.name + ' to untrack a play');
	}
}

/// Builds media array of Tracks for each video/audio in the HTML media div,
/// also writes status list of track names, in/out times, sets up callbacks
/// for all media controls and general synchronization
///
/// Given:
/// sources         -	array of objects representing media to load, like:
///	[{
/// 	"src": "path/to/file", (local or remote)
///		"type": "video" | "audio",
/// 	"name": "camera",
/// 	"in": 0.0,
/// 	"rate": 0.99955533271,
///		"syncs": [
///					4.4,
///					9.1,
///					204.2245
///				] (array of timestamps when sync points (eg. 20kHz tones) occur in this track)
/// }]
///	mediaContainer  -	DOM object into which to put the media (where videos will be played)
/// controls        -	object containing (all optional) playback controls:
/// {
///		"play": DOM button, (play/pause)
///		"scrub": DOM scrub, (sets/indicates current time)
///		"duration": DOM object, (display total duration)
///		"position": DOM object, (display current time)
/// }
/// callback        -	(optional) called when all is ready to play. Given the argument:
/// {
///		timelineLength: total length of the timeline (sec)
///	  	media: array of Track objects
/// }
function initialize(sources, mediaContainer, controls, callback) {
	if (!sources) {
		console.log('Some sources must be specified to load');
		return false;
	} else if (!mediaContainer) {
		console.log('You must specify somewhere to store the media.');
		return false;
	}

	mt$play = $(controls.play) || null;
	mt$scrub = $(controls.scrub) || null;
	mt$duration = $(controls.duration) || null;
	mt$position = $(controls.position) || null;

	var $container = $(mediaContainer);

	notLoaded = sources.length;
	var eventTimes = [];

	for (var i = 0; i < sources.length; i++) {
		var m = sources[i];
		if ( !(m.type == "video" || m.type == "audio") )  {
			console.log('Unknown media type ' + m.type);
			continue;
		}

		//TODO: sizes and media types shouldn't be hard-coded
		var $track = $('<' + m.type + ' width="1280" height="720" preload="auto">');
		if (m.type = 'video') {
			var type = "video/mp4";
		} else if (m.type = 'audio') {
			var type = "audio/wav";
		}
		var $src = $('<source src="' + m.src + /*'" type="' + type +*/ '">');

		$container.append( $track.append($src) );
		$track.one('loadedmetadata', function(m) {
			return function() {
				console.log('loaded ' + m.name)
				var t = new Track(this, m.name, m.syncs, m.in, m.rate);
				// t.ducker = duck;
				media.push(t);
				if (typeof timelineLength == 'undefined')
					timelineLength = t.out;
				else if (t.out > timelineLength)
					timelineLength = t.out;

				eventTimes.push( {
					time: t.in,
					action: 'play',
					track: t
				});
				eventTimes.push( {
					time: t.out,
					action: 'pause',
					track: t
				});
			}
		}(m) );

		$track.on('canplay', function() {
			// $('#status-' + $(this).data('name')).removeClass('unloaded');
			notLoaded--;
			if (notLoaded == 0) {	//all media fully loaded
				clearInterval(loadCheckInterval);
				updateSyncTrack();

				eventTimes.sort(function(a, b) {
					return a.time - b.time;
				});
				
				// build ordered list of deltas
				for (var i = 0; i < eventTimes.length - 1; i++) {
					var s = eventTimes[i];
					var e = eventTimes[i+1];
					var delta = new PlayStateChange(s.time, e.time, s.action, s.track);
					deltas.push(delta);
				}
				currentDelta = deltas[0];
				nextDelta = deltas.length > 1 ? deltas[1] : null;
				currentDelta.trackPlay();
				// currentlyPlaying[currentDelta.track.name] = currentDelta.track;

				if (mt$duration) mt$duration.text( asTimecode(timelineLength) );
				if (mt$scrub) mt$scrub.attr('max', timelineLength);

				if (callback)
					callback( {
						timelineLength: timelineLength,
						media: media
					});
			}
		});
	}

	playClickHandler = function() {
		play = !play;
		scrub = true;
		if (!play) updatePlayhead();
		for (var t in currentlyPlaying) {
			console.log( (play ? 'playing ' : 'pausing ') + currentlyPlaying[t].name);
			currentlyPlaying[t].dom[ play ? 'play' : 'pause' ]();
		}
		scrub = false;
		updatePlayhead();	//COMMENT OUT FOR BETTER PERFORMANCE? - not relevant if scrub is false when called.
		if (mt$play) mt$play.val( play ? 'pause' : 'play' );
	};

	if (mt$play) mt$play.click(playClickHandler);

	if (mt$scrub) {
		mt$scrub.change(function() {
			console.log('moved sync to ' + parseFloat($(this).val()));
			syncTrack.globalTime( parseFloat($(this).val()) );
			updatePlayhead();
		});
		mt$scrub.mousedown(function() {
			scrub = true;
			if (play) {
				for (var t in currentlyPlaying) {
					currentlyPlaying[t].dom.pause();
				}
			}
		});
		mt$scrub.mouseup(function() {
			syncTrack.globalTime( parseFloat($(this).val()) );
			scrub = true;
			updatePlayhead();
			scrub = false;
			if (play) {
				// What a hack!
				// Because of the delay between telling the browser to seek in a video/audio and it doing it,
				// if you tell them all to play immediately after seeking, the later ones (especially
				// if not fully loaded) will delay a moment before actually starting, causing echo.
				// Solution: just arbitrarily delay them all. This value could probably at least be calculated
				// by the number of tracks or something...
				for (var t in currentlyPlaying) {
					currentlyPlaying[t].dom.play();
					currentlyPlaying[t].dom.pause();
				}
				window.setTimeout(function() {
					for (var t in currentlyPlaying) {
						currentlyPlaying[t].dom.play();
					}
				}, 500);
			}
		});
	}

	$('body').keydown(function(event) {
		var step = 0.05;
		if (event.shiftKey)
			step *= 10;
		switch(event.keyCode) {
			case 37:		//left arrow
				scrub = true;
				syncTrack.globalTime(syncTrack.globalTime() - step);
				updatePlayhead();
				// $('#scrub').val(syncTrack.globalTime())
				scrub = false;
				break;
			case 39:		//right arrow
				scrub = true;
				syncTrack.globalTime(syncTrack.globalTime() + step);
				updatePlayhead();
				// $('#scrub').val(syncTrack.globalTime());
				scrub = false;
				break;
			case 32:		//spacebar
				playClickHandler();
				break;
		}
	});

	loadCheckInterval = setInterval(checkLoadState, 500);
}

function checkLoadState() {
	$("#media video,audio").each(function() {
		if (this.readyState > 0) {	// if running locally, the browser will load media too fast to fire load events
			$(this).triggerHandler('loadedmetadata');	// TODO: this can happen multiple times on the same track
		}
		if (this.readyState == 4)
			$(this).triggerHandler('canplay');
	});
	updateSyncTrack();
}

lastUpdate = undefined;
updateFreq = undefined;
var updatesTillUiUpdate = 0;
window.performance = window.performance || {};
performance.now = window.performance.now || Date.now;
function updatePlayhead() {		// TODO: update sync track
	var t = syncTrack.globalTime();

	if (!scrub) {	// media is playing normally; update as fast as possible
		if (nextDelta && t >= nextDelta.in) {
			// nextDelta.track.globalTime(t);
			nextDelta.apply();
			console.log('applied ' + nextDelta.action + ' to ' + nextDelta.track.name + ' at ' + t + ' (target was ' + nextDelta.in + ')');
			currentDelta = nextDelta;
			nextDelta = nextDeltaInd < deltas.length ? deltas[ ++nextDeltaInd ] : null;
			console.log('switching nextDelta from ' + currentDelta.track.name + ' to ' + nextDelta.track.name);
		}
	} else {
		// re-apply (rebase) all changes to get from here to new time, so correct tracks are playing
		while (t < currentDelta.in) {
			if (play) currentDelta.unApply();
			else currentDelta.untrackPlay();
			currentDelta.dom.currentTime = 0.0;
			console.log('skipping back');
			currentDelta = deltas[ --nextDeltaInd - 1 ]
			nextDelta = deltas[nextDeltaInd];
			if (play) currentDelta.apply();
			else currentDelta.trackPlay();
		}									// mutually exclusive; at most 1 loop will run
		while (t > currentDelta.out) {
			console.log('skipping forward from ' + currentDelta.track.name + ' to ' + nextDelta.track.name);
			currentDelta = deltas[ nextDeltaInd++ ]
			nextDelta = deltas[nextDeltaInd];
			if (play) currentDelta.apply();
			else currentDelta.trackPlay();
		}
		// sync all active tracks
		for (var track in currentlyPlaying) {
			console.log('realigning ' + track);
			track = currentlyPlaying[track];
			if (track != syncTrack) track.globalTime(t);
		}
	}

	if (updatesTillUiUpdate == 0) {
		if (mt$position) mt$position.text(asTimecode(t));
		if (mt$scrub) mt$scrub.val(t);
		updatesTillUiUpdate = 5;
	} else {
		updatesTillUiUpdate--;
	}

	if (play) window.requestAnimationFrame(updatePlayhead);
}

//find the longest currently-playing track
function updateSyncTrack() {
	var maxLen = 0;
	var newTrack = undefined;
	for (var i = media.length - 1; i >= 0; i--) {
		var track = media[i];
		if (track.in <= playhead && track.out > playhead) {
			if (track.len > maxLen) {
				newTrack = track
				maxLen = track.len;
			}
		}
	}
	if (maxLen == 0) {	//no available tracks - reached the end of playback
		if (play)
			playClickHandler();
		console.log('over');
	} else {
		// deactivate the old sync track
		if (syncTrack) {
			$(syncTrack.dom).off('timeupdate');
			$(syncTrack.dom).off('ended');
			// $('#status-' + $(syncTrack.dom).data('name')).removeClass('sync');
			console.log('sync track ' + $(syncTrack.dom).data('name') + ' ended');
		} else
			console.log('no sync track');

		syncTrack = newTrack;
		// $('#status-' + $(syncTrack.dom).data('name')).removeClass().addClass('sync');
/*		$(syncTrack.dom).on('timeupdate', function() {
			if (!scrub) {
				playhead = globalTime(this.currentTime, syncTrack);
				updatePlayhead(playhead);
				$('#scrub').val(playhead)
			}
		});*/
		$(syncTrack.dom).on('ended', function() {
			updateSyncTrack();
		});
	}
}

///////////// UTILITIES ///
function globalTime(trackSeconds, track) {
	return trackSeconds * (1/track.rate) + track.in;
}
function trackTime(globalSeconds, track) {
	return (globalSeconds - track.in) * track.rate;
}

function asTimecode(seconds) {
	var ms = Math.floor((seconds * 100) % 100);
	var s = Math.floor(seconds) % 60;
	var m = Math.floor(seconds / 60) % 60;
	var h = Math.floor(seconds / 3600);
	return two(h) + ':' + two(m) + ':' + two(s) + '.' + two(ms);
}
function two(s) {
	s += "";
	if (s.length < 2) s = "0" + s;
	return s;
}
function constrain(val, min, max) {
	return Math.min(Math.max(val, min), max)
}